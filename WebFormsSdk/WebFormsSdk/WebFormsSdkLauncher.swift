//
//  WebFormsSdkLauncher.swift
//  WebFormsSdk
//
//  Created by Movilidad Edatalia on 28/11/23.
//

import SwiftUI

@objcMembers
@objc open class WebFormsSdkLauncher: NSObject {
    @objc public static func handleDocument(jsonUrl: URL,
                                            on controller: UIViewController,
                                            callback: @escaping (_ response: WebFormsSdkIntentResult) -> ()) {
        
        var hostingController: UIHostingController<WebFormsSdkView>? = nil
        let webFormsSdkView = WebFormsSdkView(inputJsonFile: jsonUrl, completion: { response in
            hostingController?.dismiss(animated: true, completion: {
                callback(response)
            })
        })
        hostingController = UIHostingController(rootView: webFormsSdkView)
        controller.present(hostingController!, animated: true, completion: nil)
    }
}
