//
//  WebFormsSdkView.swift
//  WebFormsSdk
//
//  Created by Movilidad Edatalia on 13/11/23.
//

import SwiftUI
import WebKit

public struct WebFormsSdkView: View {
    
    static let INPUT_URI_STRING = "inputUriString"
    static let RESPONSE_ERROR_STRING = "responseErrorString"
    static let RESPONSE_URI_STRING = "responseUriString"
    static let RESPONSE_JSON_STRING = "responseJsonString"
    static let JS_CALLBACK_INTERFACE = "CallbackInterface"
    
    let inputJsonString: String
    let completion: (WebFormsSdkIntentResult) -> ()
    
    static func transformHtmlToPdf(html: String) -> URL? {
        let printFormatter = UIMarkupTextPrintFormatter(markupText: html)
        printFormatter.perPageContentInsets = UIEdgeInsets(top: 40, left: 20, bottom: 40, right: 20)
        let render = UIPrintPageRenderer()
        render.addPrintFormatter(printFormatter, startingAtPageAt: 0)
        let page = CGRect(x: 0, y: 0, width: 595.2, height: 841.8) // A4, 72 dpi
        render.setValue(page, forKey: "paperRect")
        render.setValue(page, forKey: "printableRect")
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, .zero, nil)
        for i in 0..<render.numberOfPages {
            UIGraphicsBeginPDFPage();
            render.drawPage(at: i, in: UIGraphicsGetPDFContextBounds())
        }
        UIGraphicsEndPDFContext();
        let outputURL = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent(UUID().uuidString).appendingPathExtension("pdf")
        if let outputURL = outputURL {
            try? pdfData.write(to: outputURL)
        }
        return outputURL
    }
    
    public init(inputJsonFile: URL?, completion: @escaping (WebFormsSdkIntentResult) -> ()) {
        
        self.completion = completion
        do {
            if let inputJsonFile = inputJsonFile {
                inputJsonString = try String(contentsOf: inputJsonFile)
            } else {
                let result = WebFormsSdkIntentResult(error: "Archivo no válido")
                completion(result)
                inputJsonString = ""
            }
        } catch {
            let result = WebFormsSdkIntentResult(error: "Archivo no válido")
            completion(result)
            inputJsonString = ""
            return
        }
    }
    
    public var body: some View {
        
        VStack {
            HStack {
                
                Button(action: {
                   completion(WebFormsSdkIntentResult())
                }, label: {
                    Image(systemName: "xmark")
                        .imageScale(.large)
                    
                }).padding(.horizontal)
                
                Spacer()
                
                Button(action: {
                   completion(WebFormsSdkIntentResult(rejected: true))
                }, label: {
                  Text("Rechazar")
                }).padding(.horizontal)
              
            }
            
            WebView(inputJsonString: inputJsonString, callbackHandler: { response in
                if (response.error != nil) {
                    let result = WebFormsSdkIntentResult(error: response.error)
                    completion(result)
                } else if response.html != nil {
                    let pdfFile = WebFormsSdkView.transformHtmlToPdf(html: response.html!)
                    let result = WebFormsSdkIntentResult(responseUriString: pdfFile, responseJsonString: response.jsonString)
                    completion(result)
                }
            })
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}


struct WebView: UIViewRepresentable {
    let inputJsonString: String
    let callbackHandler: (WebFormsInternalCallback) -> Void
    
    func makeUIView(context: Context) -> WKWebView {
        let webView = WKWebView()
        webView.navigationDelegate = context.coordinator
        webView.configuration.userContentController.add(context.coordinator, name: WebFormsSdkView.JS_CALLBACK_INTERFACE)
        return webView
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        let bundle = Bundle(identifier: "com.edatalia.webformssdk")!
        let url = bundle.url(forResource: "index", withExtension: "html")!
        let request = URLRequest(url: url)
        uiView.load(request)
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, WKNavigationDelegate, WKScriptMessageHandler {
        func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
            if message.name == WebFormsSdkView.JS_CALLBACK_INTERFACE,
               let messageBody = message.body as? String,
               let data = messageBody.data(using: .utf8) {
              
                do {
                    if let jsonObject = try JSONSerialization.jsonObject(with: data) as? [String : Any], let type = jsonObject["type"] as? String {
                        
                        if type == "output" {
                            let html = jsonObject["html"] as? String
                            var jsonString: String? = nil
                            if let json = jsonObject["json"] as? [[String : Any]] {
                              
                                if let jsonData = try? JSONSerialization.data(withJSONObject: json, options: [.sortedKeys]) {
                                    jsonString = String(data: jsonData, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/")
                                }
                            }
                         
                            self.parent.callbackHandler(WebFormsInternalCallback(html: html, jsonString: jsonString))
                        } else if type == "error" {
                            let error = jsonObject["value"] as? String
                            self.parent.callbackHandler(WebFormsInternalCallback(error: error))
                        }
                    }
                } catch {
                    self.parent.callbackHandler(WebFormsInternalCallback(error: error.localizedDescription))
                }
                    
            }
        }
        
        var parent: WebView
        
        init(_ parent: WebView) {
            self.parent = parent
        }
        func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            let javascript = "webForms.init(null, \(parent.inputJsonString), (result) => { window.webkit.messageHandlers.\(WebFormsSdkView.JS_CALLBACK_INTERFACE).postMessage(result); });"
            webView.evaluateJavaScript(javascript) { (result, error) in
                if let error = error {
                    self.parent.callbackHandler(WebFormsInternalCallback(error: error.localizedDescription))
                }
            }
        }
    }
}


@objc public class WebFormsSdkIntentResult: NSObject {
    @objc public var responseUriString: URL?
    @objc public var responseJsonString: String?
    @objc public var rejected: Bool
    @objc public var error: String?
    
    @objc public init(responseUriString: URL? = nil, responseJsonString: String? = nil, rejected: Bool = false, error: String? = nil) {
        self.responseUriString = responseUriString
        self.responseJsonString = responseJsonString
        self.error = error
        self.rejected = rejected
    }
}

struct WebFormsInternalCallback {
    var html: String?
    var jsonString: String?
    var error: String?
    
    public init(html: String? = nil, jsonString: String? = nil, error: String? = nil) {
        self.html = html
        self.jsonString = jsonString
        self.error = error
    }
}
