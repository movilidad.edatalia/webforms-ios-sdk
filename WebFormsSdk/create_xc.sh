rm -rf releasesXC

mkdir releasesXC

xcodebuild archive \
  -scheme WebFormsSdk \
    -archivePath releasesXC/simulator \
    -sdk iphonesimulator

xcodebuild archive \
  -scheme WebFormsSdk \
    -archivePath releasesXC/device \
    -sdk iphoneos

xcodebuild -create-xcframework \
-framework  releasesXC/device.xcarchive/Products/Library/Frameworks/WebFormsSdk.framework \
-framework  releasesXC/simulator.xcarchive/Products/Library/Frameworks/WebFormsSdk.framework \
-output  releasesXC/WebFormsSdk.xcframework

