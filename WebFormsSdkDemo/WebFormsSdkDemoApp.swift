//
//  WebFormsSdkDemoApp.swift
//  WebFormsSdkDemo
//
//  Created by Movilidad Edatalia on 13/11/23.
//

import SwiftUI
import WebFormsSdk
import UniformTypeIdentifiers


@main
struct WebFormsSdkDemoApp: App {
    
    @State private var isFileImportedPresented = false
    @State private var isSdkPresented = false
    @State var url: URL? = nil
    
    var body: some Scene {
        WindowGroup {
                if (isSdkPresented) {
                    WebFormsSdkView(inputJsonFile: url, completion: { result in
                        url?.stopAccessingSecurityScopedResource()
                        isSdkPresented = false
                        
                        if let error = result.error {
                            print(error)
                        }
                        
                        if let formValuesString = result.responseJsonString {
                            print(formValuesString)
                        }
                        
                        if let pdfUrl = result.responseUriString {
                            
                            let activityViewController = UIActivityViewController(activityItems: [pdfUrl], applicationActivities: nil)
                            
                            if let viewController = UIApplication.shared.windows.first?.rootViewController {
                                
                                if let popoverController = activityViewController.popoverPresentationController {
                                    popoverController.sourceView = viewController.view
                                    popoverController.sourceRect = CGRect(x: 0, y: 0, width: 200, height: 200) // Rectángulo en la vista fuente
                                }
                                
                                viewController.present(activityViewController, animated: true, completion: nil)
                            }
                        }
                    })
                } else {
                    VStack(alignment: .center) {
                        Button(action: {
                            isFileImportedPresented.toggle()
                        }) {
                            Text("Seleccionar configuración (.webform)")
                        }.fileImporter(isPresented: $isFileImportedPresented, allowedContentTypes: [ UTType(filenameExtension: "webform")! ], allowsMultipleSelection: false, onCompletion: { result in
                            switch result {
                            case .success(let fileUrls):
                                url = fileUrls.first
                                _ = url?.startAccessingSecurityScopedResource()
                                isSdkPresented = true
                            case .failure(let error):
                                print(error)
                            }
                        })
                    }
                }
        }
    }
}
